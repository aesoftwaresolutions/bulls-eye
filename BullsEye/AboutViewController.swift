//
//  AboutViewController.swift
//  BullsEye
//
//  Created by Anthony Murdukhayev on 10/26/14.
//  Copyright (c) 2014 Anthony Murdukhayev. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {
    
    @IBAction func Close(){
        dismissViewControllerAnimated(true, completion: nil)
    }
}
