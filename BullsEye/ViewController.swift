//
//  ViewController.swift
//  BullsEye
//
//  Created by Anthony Murdukhayev on 10/26/14.
//  Copyright (c) 2014 Anthony Murdukhayev. All rights reserved.

import UIKit

class ViewController: UIViewController {
    var currentValue = 50
    var targetValue = 0
    var score = 0
    var round = 0
    
    @IBOutlet weak var targetLabel: UILabel!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var roundLabel: UILabel!
    @IBOutlet weak var startOverButton: UIButton!
    
    
    @IBAction func startNewGame(){
        startNewRound()
        targetValue = 0
        score = 0
        round = 0
        updateLabels()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startNewRound()
        
        let thumbImageNormal = UIImage(named: "SliderThumb-Normal")
        slider.setThumbImage(thumbImageNormal, forState: .Normal)
        
        let thumbImageHighlighted = UIImage(named: "SliderThumb-Highlighted")
        slider.setThumbImage(thumbImageHighlighted, forState: .Highlighted)
        
        let insets = UIEdgeInsets(top: 0, left: 14, bottom: 0, right: 14)
        
        if let trackLeftImage = UIImage(named: "SliderTrackLeft") {
            let trackLeftResizable = trackLeftImage.resizableImageWithCapInsets(insets)
            slider.setMinimumTrackImage(trackLeftResizable, forState: .Normal)
        }
        
        if let trackRightImage = UIImage(named: "SliderTrackRight"){
            let trackRightResizable = trackRightImage.resizableImageWithCapInsets(insets)
            slider.setMaximumTrackImage(trackRightResizable, forState: .Normal)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func showAlert(){
        var title: String
        
        let difference = abs(currentValue - targetValue)
        let points = 100 - difference
        
        if difference == 0 {
            score += 100
            title = "Perfect!"
        } else if difference <= 5 {
            score += 50
            title = "Almost There"
        } else {
            title = "Not Even Close"
        }
        
        score += points
        
        let message =  "You scored: \(points) points"//+"\nThe value of the slider is: \(currentValue)" + "\nThe Target Value is: \(targetValue)" + "\nThe Difference is: \(difference)"
        let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        let action = UIAlertAction(title: "OK", style: .Default, handler: {
            action in
            self.startNewRound()
            }
        )
        alert.addAction(action)
        presentViewController(alert, animated: true, completion: nil)
        //startNewRound()
    }
    
    @IBAction func sliderMoved(slider: UISlider){
        currentValue = lroundf(slider.value)
    }
    
    func startNewRound(){
        round+=1
        targetValue = 1 + Int(arc4random_uniform(100))
        currentValue = 50
        slider.value = Float(currentValue)
        updateLabels()
    }
    
    func updateLabels(){
        targetLabel.text = String(targetValue)
        scoreLabel.text = String(score)
        roundLabel.text = String(round)
    }
    
}

